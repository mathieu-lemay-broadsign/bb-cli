import logging
from urllib.parse import parse_qs, quote, urljoin, urlparse

import requests

from . import config

API_URL_V2_REPOSITORIES = "https://api.bitbucket.org/2.0/repositories/"
logger = logging.getLogger(__name__)


def _build_url(base_url, repo, *args, slash_terminated=False):
    url = urljoin(base_url, "/".join([config.workspace, repo] + list(args)))

    if slash_terminated and url[-1] != "/":
        url += "/"

    return url


def _get_auth():
    return config.username, config.password


def _json_or_raise(resp):
    if not resp.ok:
        raise Exception(resp.text)

    return resp.json()


def _request(method, url, *args, **kwargs):
    logger.debug("%s %s (%s, %s)", method, url, args, kwargs)
    resp = requests.request(method, url, auth=_get_auth(), **kwargs)
    return _json_or_raise(resp)


def _get(url, *args, **kwargs):
    return _request("GET", url, *args, **kwargs)


def _paginated_get(url, *args, page=1, pagelen=50, **kwargs):
    values = []
    params = {"page": page, "pagelen": pagelen}

    if "params" in kwargs:
        params.update(kwargs["params"])

    while True:
        resp = _get(url, *args, params=params)

        values.extend(resp["values"])

        logger.debug("NB Values: %d, Size: %d", len(values), resp["size"])
        if len(values) >= resp["size"]:
            break

        next_ = resp.get("next")
        logger.debug("Next: %s", next_)
        if next_:
            qs = urlparse(next_).query
            if not qs:
                break

            params = parse_qs(qs)
            for k, v in params.items():
                params[k] = v[0] if isinstance(v, list) and len(v) == 1 else v
        else:
            params["page"] += 1

    return values


def _post(url, *args, **kwargs):
    return _request("POST", url, *args, **kwargs)


def _put(url, *args, **kwargs):
    return _request("PUT", url, *args, **kwargs)


def get_repository_variables(repo):
    url = _build_url(API_URL_V2_REPOSITORIES, repo, "pipelines_config", "variables", slash_terminated=True)
    return _paginated_get(url)


def set_repository_variables(repo, variables):
    current_vars = {v["key"]: v for v in get_repository_variables(repo)}

    create_url = _build_url(API_URL_V2_REPOSITORIES, repo, "pipelines_config", "variables", slash_terminated=True)

    for v in variables:
        v["secured"] = v.get("secured", False)

        k = v["key"]
        if k in current_vars:
            var_uuid = current_vars[k]["uuid"]
            update_url = _build_url(
                API_URL_V2_REPOSITORIES,
                repo,
                "pipelines_config",
                "variables",
                quote(var_uuid),
            )
            _put(update_url, json=v)
        else:
            _post(create_url, json=v)


def get_environment(repo, uuid):
    url = _build_url(API_URL_V2_REPOSITORIES, repo, "environments", quote(f"{{{uuid}}}"))
    return _get(url)


def get_environments(repo):
    url = _build_url(API_URL_V2_REPOSITORIES, repo, "environments", slash_terminated=True)
    return _paginated_get(url)


def _get_env_uuid(repo, env_uuid, env_name):
    if env_uuid:
        return f"{env_uuid}"

    if not env_name:
        raise ValueError("One of `name` and `uuid` must be specified")

    envs = get_environments(repo)
    env_uuid = next((e["uuid"] for e in envs if e["name"] == env_name), None)

    if not env_uuid:
        raise ValueError(f"Invalid environment: {env_name}")

    return env_uuid[1:-1]


def get_environment_variables(repo, env_uuid, env_name):
    env_uuid = _get_env_uuid(repo, env_uuid, env_name)

    url = _build_url(
        API_URL_V2_REPOSITORIES, repo, "deployments_config", "environments", quote(f"{{{env_uuid}}}"), "variables"
    )
    return _paginated_get(url)


def set_environment_variables(repo, env_uuid, env_name, variables):
    env_uuid = _get_env_uuid(repo, env_uuid, env_name)

    current_vars = {v["key"]: v for v in get_environment_variables(repo, env_uuid, None)}

    create_url = _build_url(
        API_URL_V2_REPOSITORIES, repo, "deployments_config", "environments", quote(f"{{{env_uuid}}}"), "variables"
    )

    for v in variables:
        v["secured"] = v.get("secured", False)

        k = v["key"]
        if k in current_vars:
            var_uuid = current_vars[k]["uuid"]
            update_url = _build_url(
                API_URL_V2_REPOSITORIES,
                repo,
                "deployments_config",
                "environments",
                quote(f"{{{env_uuid}}}"),
                "variables",
                quote(var_uuid),
            )
            _put(update_url, json=v)
        else:
            _post(create_url, json=v)


def run_custom_pipeline(repo, pipeline_name, branch, commit, variables):
    variables = [{"key": k, "value": v} for k, v in map(lambda x: x.split("="), variables)]

    create_url = _build_url(API_URL_V2_REPOSITORIES, repo, "pipelines", slash_terminated=True)

    if branch and commit:
        target = {
            "type": "pipeline_ref_target",
            "ref_type": "branch",
            "ref_name": branch,
            "commit": {"type": "commit", "hash": commit},
        }
    elif branch:
        target = {
            "type": "pipeline_ref_target",
            "ref_type": "branch",
            "ref_name": branch,
        }
    elif commit:
        target = {
            "type": "pipeline_commit_target",
            "commit": {
                "type": "commit",
                "hash": commit,
            },
        }
    else:
        raise ValueError("branch and/or commit must be specified")

    target["selector"] = {"type": "custom", "pattern": pipeline_name}

    payload = {
        "target": target,
        "variables": variables,
    }

    _post(create_url, json=payload)


def get_pipelines(repo):
    url = _build_url(API_URL_V2_REPOSITORIES, repo, "pipelines/")
    return _paginated_get(url)


def get_pull_requests(repo, state):
    all_states = ["OPEN", "MERGED", "DECLINED", "SUPERSEDED"]

    state = state.upper()
    if state == "ALL":
        params = {"state": all_states}
    elif state in all_states:
        params = {"state": state}
    else:
        raise ValueError(f"Invalid state: {state}")

    url = _build_url(API_URL_V2_REPOSITORIES, repo, "pullrequests")
    return _paginated_get(url, params=params)
