import json
import logging

import click

from . import bitbucket as bb


def _setup_logger():
    h = logging.StreamHandler()
    h.setFormatter(
        logging.Formatter(
            fmt="%(asctime)s.%(msecs)03d [%(levelname)-8s] %(name)s: %(message)s", datefmt="%Y-%m-%d %H:%M:%S"
        )
    )

    logger = logging.getLogger("bb_cli")
    logger.setLevel(logging.DEBUG)
    logger.handlers.append(h)


@click.group()
@click.option("-r", "--repo", required=True, help="Repository")
@click.pass_context
def main(ctx, repo):
    _setup_logger()

    ctx.ensure_object(dict)

    ctx.obj["repo"] = repo


@main.command()
@click.pass_context
def get_repository_variables(ctx):
    values = bb.get_repository_variables(**ctx.obj)

    print(json.dumps(values))


@main.command()
@click.option("-f", "--file", "vars_file", required=True, help="JSON File containing the variables")
@click.pass_context
def set_repository_variables(ctx, vars_file):
    with open(vars_file) as f:
        variables = json.load(f)

    bb.set_repository_variables(**ctx.obj, variables=variables)


@main.command()
@click.option("-i", "--uuid", required=True, help="The environment's UUID")
@click.pass_context
def get_environment(ctx, uuid):
    value = bb.get_environment(**ctx.obj, uuid=uuid)

    print(json.dumps(value))


@main.command()
@click.pass_context
def get_environments(ctx):
    values = bb.get_environments(**ctx.obj)

    print(json.dumps(values))


@main.command()
@click.option("-i", "--uuid", help="The environment's UUID")
@click.option("-n", "--name", help="The environment's name")
@click.pass_context
def get_environment_variables(ctx, uuid, name):
    values = bb.get_environment_variables(**ctx.obj, env_uuid=uuid, env_name=name)

    print(json.dumps(values))


@main.command()
@click.option("-f", "--file", "vars_file", required=True, help="JSON File containing the variables")
@click.option("-i", "--uuid", help="The environment's UUID")
@click.option("-n", "--name", help="The environment's name")
@click.pass_context
def set_environment_variables(ctx, vars_file, uuid, name):
    with open(vars_file) as f:
        variables = json.load(f)

    bb.set_environment_variables(**ctx.obj, env_uuid=uuid, env_name=name, variables=variables)


@main.command()
@click.argument("pipeline-name")
@click.option("-b", "--branch", help="Target branch")
@click.option("-c", "--commit", help="Target branch")
@click.option("-v", "--var", "variables", multiple=True, help="Pipeline variables")
@click.pass_context
def run_custom_pipeline(ctx, pipeline_name, branch, commit, variables):
    bb.run_custom_pipeline(**ctx.obj, pipeline_name=pipeline_name, branch=branch, commit=commit, variables=variables)


@main.command()
@click.pass_context
def get_pipelines(ctx):
    values = bb.get_pipelines(**ctx.obj)

    print(json.dumps(values))


@main.command()
@click.pass_context
@click.argument("state", default="all")
def get_pull_requests(ctx, state):
    values = bb.get_pull_requests(**ctx.obj, state=state)

    print(json.dumps(values))
