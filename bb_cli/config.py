import logging
import os
import sys

import tomlkit
import xdg

logger = logging.getLogger(__name__)

DEFAULT_CONFIG = """
[bb-cli]
username = ""
password = ""
workspace = ""
"""


class Config:
    def __init__(self):
        self._username = None
        self._password = None
        self._workspace = None

    @property
    def username(self):
        return self._username

    @property
    def password(self):
        return self._password

    @property
    def workspace(self):
        return self._workspace

    @classmethod
    def load(cls):
        conf = cls()

        conf._load_defaults()

        fp = os.path.join(xdg.xdg_config_home(), "bb-cli", "config.toml")
        logger.debug("Loading config file: %s", fp)

        if os.path.exists(fp):
            conf._load_from_file(fp)
        else:
            logger.debug("File not found: %s", fp)

        conf._load_from_env()

        conf._validate()

        return conf

    def _load_defaults(self):
        self._load_toml(DEFAULT_CONFIG)

    def _load_from_file(self, fp: str):
        with open(fp) as f:
            self._load_toml(f.read())

    def _load_from_env(self):
        for k in ("username", "password", "workspace"):
            envvar_name = f"BB_CLI_{k.upper()}"
            if envvar_name in os.environ:
                setattr(self, f"_{k}", os.environ[envvar_name])

    def _load_toml(self, toml_data: str):
        values = tomlkit.parse(toml_data)

        bb_cli_values = values.get("bb-cli")
        if not bb_cli_values:
            return

        for k in ("username", "password", "workspace"):
            if k in bb_cli_values:
                setattr(self, f"_{k}", str(bb_cli_values[k]))

    def _validate(self):
        missing = []
        for k in ("username", "password", "workspace"):
            if not getattr(self, f"_{k}"):
                missing.append(k)

        if missing:
            raise ValueError(f"Missing configurations: {missing}")


_conf = Config.load()

sys.modules[__name__] = _conf
