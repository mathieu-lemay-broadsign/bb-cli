app := "bb_cli"

run *args: _deps
	poetry run python -m {{ app }} {{ args }}

lint:
	pre-commit run --all

clean:
	rm -f .make.*

@_deps:
	[[ ! -f .make.poetry || poetry.lock -nt .make.poetry ]] && ( poetry install; touch .make.poetry ) || true

# vim: ft=make
